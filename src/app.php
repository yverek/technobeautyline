<?php

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\Loader\YamlFileLoader;
use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Symfony\Component\Translation\Loader\XliffFileLoader;

use Silex\Provider\FormServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;
use Silex\Provider\ValidatorServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Silex\Provider\TranslationServiceProvider;
use Silex\Provider\SwiftmailerServiceProvider;

$app = new Silex\Application();

$app['config.locales'] = array('it', 'en');
$app['config.locales.regexp'] = 'it|en';

$app->register(new FormServiceProvider());
$app->register(new UrlGeneratorServiceProvider());
$app->register(new ValidatorServiceProvider());
$app->register(new SessionServiceProvider());
$app->register(new TwigServiceProvider(), array(
    'twig.path' => array(__DIR__ . '/../templates',),
    'twig.options' => array('cache' => __DIR__ . '/../var/cache/twig',),
));
$app->register(new SwiftmailerServiceProvider(), array(
     'swiftmailer.options' => array(
            'host' => 'smtp.gmail.com',
            'port' => 465,
            'username' => 'mau.dattilo@gmail.com',
            'password' => '.:T3mp0:.',
            'encryption' => 'ssl',
            'auth_mode' => 'login'),
      'swiftmailer.class_path' => __DIR__.'/../vendor/swiftmailer/lib/classes'
));

/**
 * Before filter setting language from URL
 */
$app->before(function () use ($app) {
    /**
     * This must be done here as access to $app['request'] is restricted outside this scope
     */
    if ($locale = $app['request']->get('locale')) {
        $app['locale'] = $locale;
        $app['session']->set('locale', $locale);
    }

    /**
     * Translations must be set AFTER configuring $app['locale']
     */
    $app->register(new TranslationServiceProvider(), array(
        'locale_fallbacks' => array('it'), // Default locale
    ));

    $app['translator'] = $app->share($app->extend('translator', function($translator, $app) {
        $translator->addLoader('yaml', new YamlFileLoader());
        $translator->addLoader('xlf', new XliffFileLoader());

        $translator->addResource('yaml', __DIR__ . '/../locales/it.yml', 'it');
        $translator->addResource('yaml', __DIR__ . '/../locales/en.yml', 'en');
        $translator->addResource('xlf', __DIR__.'/../vendor/symfony/validator/Symfony/Component/Validator/Resources/translations/validators.it.xlf', 'it', 'validators');
        $translator->addResource('xlf', __DIR__.'/../vendor/symfony/validator/Symfony/Component/Validator/Resources/translations/validators.en.xlf', 'en', 'validators');
        return $translator;
    }));

    /**
     * Translator must be sent to Twig after setting up everything
     */
    $app['twig']->addExtension(new TranslationExtension($app['translator']));

    /**
     * Global Twig templates must be defined once everything is set
     */
    $app['twig']->addGlobal('layout', $app['twig']->loadTemplate('layout.html'));
});

return $app;