<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Index URL, automatic redirect to preferred user locale
 */
$app->get('/', function () use ($app) {
    $locale = $app['request']->getPreferredLanguage($app['config.locales']);

    return $app->redirect($app['url_generator']->generate('homepage', array('_locale' => $locale)));
});

$locale = $app['controllers_factory'];

$locale->get('/', function () use ($app){
    return $app['twig']->render('home.html');
})
->bind('homepage')
->assert('locale', $app['config.locales.regexp']);

/**
 * All products routes
 */
$locale->get('/products', function () use ($app) {
  return $app['twig']->render('products/home.html');
})
->bind('products');

$locale->get('/products/body-impact', function () use ($app) {
  return $app['twig']->render('products/body-impact.html');
})
->bind('products_body_impact');

/*$locale->get('/products/purity-visage', function () use ($app) {
  return $app['twig']->render('products/purity-visage.html');
})
->bind('products_purity_visage');

$locale->get('/products/skin-program', function () use ($app) {
  return $app['twig']->render('products/skin-program.html');
})
->bind('products_skin_program');

$locale->get('/products/thalaxoterm', function () use ($app) {
  return $app['twig']->render('products/thalaxoterm.html');
})
->bind('products_thalaxoterm');*/

/**
 * All company routes
 */
/*$locale->get('/company', function () use ($app) {
  return $app['twig']->render('company/home.html');
})
->bind('company');*/

$locale->match('/contacts', function (Request $request) use($app) {
  $form = $app['form.factory']->createBuilder('form')
          ->add('name', 'text', array(
              'required'    => false,
              'label'       => 'contact.form.label.name',
              'constraints' => new Assert\NotBlank(),
              'attr'        => array('placeholder' => 'contact.form.placeholder.name')))
          ->add('surname', 'text', array(
              'required'    => false,
              'label'       => 'contact.form.label.surname',
              'constraints' => new Assert\NotBlank(),
              'attr'        => array('placeholder' => 'contact.form.placeholder.surname')))
          ->add('email', 'text', array(
              'required'    => false,
              'label'       => 'contact.form.label.email',
              'constraints' => array(new Assert\NotBlank(), new Assert\Email()),
              'attr'        => array('placeholder' => 'contact.form.placeholder.email')))
          ->add('subject', 'text', array(
              'required'    => false,
              'label'       => 'contact.form.label.subject',
              'constraints' => new Assert\NotBlank(),
              'attr'        => array('placeholder' => 'contact.form.placeholder.subject')))
          ->add('message', 'textarea', array(
              'required'    => false,
              'label'       => 'contact.form.label.message',
              'constraints' => new Assert\NotBlank(),
              'attr'        => array('placeholder' => 'contact.form.placeholder.message')))
          ->getForm();

  $form->handleRequest($request);

  if ($form->isValid()) {
    $data = $form->getData();
    $body = $app['twig']->render('mail.html', array(
                                       'name'     => $data['name'],
                                       'surname'  => $data['surname'],
                                       'email'    => $data['email'],
                                       'subject'  => $data['subject'],
                                       'message'  => $data['message'],
                                       'ip'       => $request->getClientIp()));
    $message = \Swift_Message::newInstance()
                ->setSubject($data['subject'])
                ->setFrom(array($data['email'] => $data['name'] . ' ' . $data['surname']))
                ->setTo(array('mau.dattilo@gmail.com'))
                ->setBody($body, 'text/html');

    $app['mailer']->send($message);
    $app['session']->getFlashBag()->add('success', 'true');

    return $app->redirect($app['url_generator']->generate('contacts'));
  }

  return $app['twig']->render('contacts.html', array('form' => $form->createView()));
})
->bind('contacts')
->method('GET|POST');

/*$locale->get('/privacy', function () use($app) {
  return $app['twig']->render('company/privacy.html');
})
->bind('privacy');

$locale->get('/terms', function () {
  return $app['twig']->render('terms.html');
})
->bind('terms');

$locale->get('/credits', function () {
  return $app['twig']->render('credits.html');
})
->bind('credits');

$locale->get('/sitemap', function () {
  return $app['twig']->render('sitemap.html');
})
->bind('sitemap');*/

$app->error(function (\Exception $e, $code) use ($app) {
  return $app->redirect('/');
});

$app->mount('/{_locale}', $locale);